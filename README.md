# bookdata

8586 unique book metadata entries in JSON format. Sourced and cleaned up from https://github.com/zygmuntz/goodbooks-10k/ and augmented with data from https://openlibrary.org/

Sample:

```json
  {
    "goodreads_id": 960,
    "title": "Angels & Demons ",
    "isbn10": "1416524797",
    "isbn13": "9781416524793",
    "authors": [
      "Dan Brown"
    ],
    "year": "2000",
    "page_count": 713,
    "tags": [
      "fiction",
      "mystery",
      "thriller",
      "dan-brown",
      "suspense",
      "series",
      "adventure",
      "mystery-thriller",
      "historical-fiction",
      "crime",
      "adult",
      "thrillers",
      "contemporary"
    ],
    "subjects": [
      "Physicists -- Crimes against -- Fiction",
      "Religious educators -- Fiction",
      "Popes -- Election -- Fiction",
      "Signs and symbols -- Fiction",
      "Secret societies -- Fiction",
      "Anti-Catholicism -- Fiction",
      "Illuminati -- Fiction",
      "Vendetta -- Fiction",
      "Vatican City -- Fiction"
    ],
    "description": "World-renowned Harvard symbologist Robert Langdon is summoned to a Swiss research facility to analyze a cryptic symbol seared into the chest of a murdered physicist. What he discovers is unimaginable: a deadly vendetta against the Catholic Church by a centuries-old underground organization -- the Illuminati. In a desperate race to save the Vatican from a powerful time bomb, Langdon joins forces in Rome with the beautiful and mysterious scientist Vittoria Vetra. Together they embark on a frantic hunt through sealed crypts, dangerous catacombs, and deserted cathedrals, and into the depths of the most secretive vault on earth...the long-forgotten Illuminati lair.\r\n(back cover)",
    "publishers": [
      "Pocket Books"
    ]
  },
```
